Project to implement/showcase some pathfinding algorithms in Python.

It currently only contains one algorithm (A star) but might be expanded later.

# A star algorithm

It is an adaptation of the pseudo code version available
on [wikipedia](https://fr.wikipedia.org/wiki/Algorithme_A*#Pseudo-code)

I added a parameter to change the cost of traveling through a diagonal
to reflect a more realistic path. It is set to 1.4 (close to the square root of 2).
