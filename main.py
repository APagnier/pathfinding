'''
Module to handle A* algorithm, it is inspired from wikipedia pseudocode.
'''

from node import Grid

DEBUG = False
GRID_SIZE_X = 8
GRID_SIZE_Y = 8
ADD_DIAG_WEIGHT = 0.4
PRINT_OUTPUT = True


class PathError(Exception):
    '''
    An error occured while concustring the path.
    '''


def debug(string):
    '''
    Quick function to easily print some debug
    '''
    if DEBUG:
        print(string)


def least_costly_neighbor(grid, node):
    '''
    Returns the nearest neighbor of the node to the start.
    NB: it will return the node if no neighbors are closer to the start.
    '''
    min_cost = grid.get_node(node).get_dist()
    nearest = node
    for new_node in grid.get_neighbor(node):
        dist = grid.get_node(new_node).get_dist()
        if dist is not None and dist < min_cost:
            nearest = new_node
            min_cost = grid.get_node(new_node).get_dist()
    return nearest


def build_path(grid, end, start, wall_list, print_path=False):
    '''
    A path between start and end has been calculated,
    we can reconstruct it from the end by following
    the cost of the lowest node at each step.

    Args:
        grid: the full grid with calculated nodes
        end: (int, int) coordinate of end
        start: (int, int) coordinate of start
        wall_list: (List of (int, int)) list of coordinate of walls
        print_path: (bool) print or not on console the path
    Returns:
        list_path: (List of (int, int)) path from end to start
    '''
    if print_path:
        print("Rebuild path")
    list_path = [end]
    current_node = end
    while current_node != start:
        debug(current_node)
        new_current_node = least_costly_neighbor(grid, current_node)
        if current_node == new_current_node:
            raise PathError()
        list_path.append(new_current_node)
        current_node = new_current_node
    if print_path:
        for j in range(GRID_SIZE_Y):
            for i in range(GRID_SIZE_X):
                if (i, j) == end:
                    print("E", end="")
                elif (i, j) == start:
                    print("S", end="")
                elif (i, j) in list_path:
                    print("X", end="")
                elif (i, j) in wall_list:
                    print("W", end="")
                else:
                    print(" ", end="")
            print("")
    return list_path


def get_dist_in_list(list_to_check, node, grid):
    '''
    Returns the distance of the node if it is present in the list
    otherwise returns -1.
    '''
    if node not in list_to_check:
        return -1
    dist = grid.get_node(node).get_dist()
    if dist is None:
        return -1
    return dist


def calc_a_star(grid_size, start, end, walls):
    '''
    Compute a path between "start" and "end" using A* algorithm
    '''
    grid = Grid(grid_size[0], grid_size[1])
    grid.get_node(start).set_dist(0)
    open_list = [start]
    close_list = []
    def dist_to_end(node):
        '''
        return the square of the distance between nodes:
        we don't need the distance itself to order nodes
        '''
        x = node[0] - end[0]
        y = node[1] - end[1]
        return x*x + y*y

    for i, j in walls:
        grid.set_wall(i, j)

    while len(open_list) != 0:
        debug(open_list)
        open_list.sort(key=dist_to_end)  # sort the list using heuristic
        node = open_list[0]  # take the first element of the list
        # we could improve this part by directly inserting elements at the right place
        # that way the list would be already sorted and we would gain speed
        if node == end:  # we reached end, let's build the path
            build_path(grid, end, start, walls, print_path=PRINT_OUTPUT)
            return
        # check all neighboor of the current node and calc the cost of their path
        for new_node in grid.get_neighbor(node):
            debug("  "+str(new_node))
            if new_node not in close_list:  # the node is not already done
                new_dist = get_dist_in_list(open_list, new_node, grid)
                dist = grid.get_node(node).get_dist()
                debug("    "+str(new_dist)+"/"+str(dist))
                add_weight = 1
                if new_node[0] != node[0] and new_node[1] != node[1]:  # it is a diag
                    add_weight += ADD_DIAG_WEIGHT
                # check if the new distance calculated is smaller than older ones
                if new_dist == -1 or new_dist > dist + add_weight:
                    grid.get_node(new_node).set_dist(dist + add_weight)
                    open_list.append(new_node)
        # the current node is fully checked, we add it to the list of fully checked nodes
        open_list.remove(node)
        close_list.append(node)
    raise PathError()


def test():
    '''
    Let's test the module with a wall in the middle of the way.
    '''
    start = (2, 2)
    end = (6, 6)
    wall_list = [(4, 4), (3, 4), (5, 4),
                 (5, 3), (5, 2), (2, 4)]
    calc_a_star( (GRID_SIZE_X, GRID_SIZE_Y) , start, end, wall_list)


if __name__ == "__main__":
    test()
