

class Node():
    def __init__(self):
        self._dist = None
        self._heur = None
        self._walkable = True

    def set_dist(self, dist):
        self._dist = dist

    def get_dist(self):
        return self._dist

    def set_heur(self, heur):
        self._heur = heur

    def get_heur(self):
        return self._heur

    def set_as_wall(self):
        self._walkable = False

    def get_walkable(self):
        return self._walkable


class NodeLink():
    def __init__(self, cost):
        self._list_neighbor = []
        self._dist = None
        self._cost = cost

    def add_neighbor(self, node, cost=1):
        self._list_neighbor.append((node, cost))

    def get_neighbors(self):
        return self._list_neighbor

    def set_dist(self, dist):
        self._dist = dist

    def get_dist(self):
        return self._dist


class Grid():
    def __init__(self, x, y):
        self._grid = []
        self._x = x
        self._y = y
        for i in range(x):
            self._grid.append([])
            for _ in range(y):
                self._grid[i].append(Node())

    def set_wall(self, x, y):
        self.get_node((x, y)).set_as_wall()

    def get_neighbor(self, coord):
        res = []
        x, y = coord
        for i, j in [(x-1, y-1),
                     (x-1, y),
                     (x-1, y+1),
                     (x, y-1),
                     (x, y+1),
                     (x+1, y-1),
                     (x+1, y),
                     (x+1, y+1)]:
            if self.get_node((i, j)) is not None and self.get_node((i, j)).get_walkable():
                res.append((i, j))
        return res

    def get_node(self, coord):
        x, y = coord
        if x < 0 or y < 0 or x >= self._x or y >= self._y:
            return None
        return self._grid[x][y]
